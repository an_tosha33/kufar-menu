"use strict";

$(window).on('load', function () {
  var throttle = function throttle(func, ms) {
    var isThrottled = false;
    var savedArgs;
    var savedThis;

    function wrap() {
      if (isThrottled) {
        savedArgs = arguments;
        savedThis = this;
        return;
      }

      func.apply(this, arguments);
      isThrottled = true;
      setTimeout(function () {
        isThrottled = false;

        if (savedArgs) {
          wrap.apply(savedThis, savedArgs);
          savedArgs = savedThis = null;
        }
      }, ms);
    }

    return wrap;
  };

  var debounce = function debounce(func, ms) {
    var isCooldown = false;
    return function () {
      if (isCooldown) return;
      func.apply(this, arguments);
      isCooldown = true;
      setTimeout(function () {
        return isCooldown = false;
      }, ms);
    };
  };

  var hoverHanler = function hoverHanler(el) {
    $('.nav-list .nav-item').removeClass('active');
    el.addClass('active');
  };

  var pageX = 0;
  $('.nav-list').mousemove(function (ev) {
    if (pageX == 0) pageX = ev.pageX;
    setTimeout(function () {
      pageX = ev.pageX;
    }, 150);

    if (ev.pageX > pageX + 5) {
      return;
    } else {
      if ($(ev.target).hasClass('nav-item')) {
        hoverHanler($(ev.target));
      } else if ($(ev.target).hasClass('nav-item-name')) {
        hoverHanler($(ev.target).closest('.nav-item'));
      }
    }
  });
});