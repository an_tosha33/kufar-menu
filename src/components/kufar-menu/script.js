$(window).on('load', function () {

	const throttle = (func, ms) => {
		let isThrottled = false;
		let savedArgs;
		let savedThis;

		function wrap() {
			if (isThrottled) {
				savedArgs = arguments;
				savedThis = this;
				return;
			}

			func.apply(this, arguments);

			isThrottled = true;

			setTimeout(() => {
				isThrottled = false;
				if (savedArgs) {
					wrap.apply(savedThis, savedArgs);
					savedArgs = savedThis = null;
				}
			}, ms)


		}
		return wrap;
	}

	const debounce = (func, ms) => {
		let isCooldown = false;
		return function () {
			if (isCooldown) return;

			func.apply(this, arguments);

			isCooldown = true;

			setTimeout(() => isCooldown = false, ms);
		};
	}


	const hoverHanler = (el) => {
		$('.nav-list .nav-item').removeClass('active');
		el.addClass('active');
	}

	let pageX = 0;

	$('.nav-list').mousemove(function (ev) {
		if (pageX == 0) pageX = ev.pageX;
		setTimeout(() => {
			pageX = ev.pageX;
		}, 150)
		if (ev.pageX > pageX + 5) {
			return;
		} else {
			if ($(ev.target).hasClass('nav-item')) {
				hoverHanler($(ev.target));
			} else if ($(ev.target).hasClass('nav-item-name')) {
				hoverHanler($(ev.target).closest('.nav-item'));
			}
		}
	})
})